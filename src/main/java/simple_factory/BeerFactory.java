package simple_factory;

import beer.Beer;
import beer.Blackbeer;
import beer.Wheatbeer;

/**
 * Description
 *
 * @author Tom Dittrich duke64@gmx.de
 * @version 0.1
 */
public class BeerFactory {

    public Beer createBeer(String kindOfBeer) {
        switch (kindOfBeer) {
            case "Blackbeer":
                return new Blackbeer();
            case "Wheatbeer":
                return new Wheatbeer();
            default:
                return null;
        }
    }
}
