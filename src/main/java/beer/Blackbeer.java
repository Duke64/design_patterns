package beer;

/**
 * Description
 *
 * @author Tom Dittrich duke64@gmx.de
 * @version 0.1
 */
public class Blackbeer implements Beer {

    private final String NAME = "Blackbeer";

    public String getName() {
        return NAME;
    }
}
