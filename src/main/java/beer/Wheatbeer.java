package beer;

/**
 * Description
 *
 * @author Tom Dittrich duke64@gmx.de
 * @version 0.1
 */
public class Wheatbeer implements Beer {

    private final String NAME = "Wheatbeer";

    public String getName() {
        return NAME;
    }
}
